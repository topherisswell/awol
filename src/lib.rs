use regex::Regex;
use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::{thread, time};

pub struct Config {
    path: String,
    interval: u64,
}

impl Config {
    pub fn new() -> Config {
        // TODO - inherit config_path from command-line params
        let path = "targets".to_string(); // the path to the file with the MAC addresses to be targeted
        let interval = 11; //time to wait before sending another wave of WOL packets
        Config { path, interval }
    }
}

// entrypoint is here. Repeatedly open the config file, send magic packets, and wait
pub fn run(config: Config) {
    // -> Result<(), Box<dyn Error>> {
    let wait_time = time::Duration::from_secs(config.interval);
    loop {
        read_config(&config.path);
        thread::sleep(wait_time);
    }
}

// takes a byte array and sends a WOL magic packet to that MAC.
// capable of returning dynamic errors.
pub fn send_magic_packet(mac_address: [u8; 6]) -> Result<(), Box<dyn Error>> {
    // Create a magic packet (but don't send it yet)
    let magic_packet = wake_on_lan::MagicPacket::new(&mac_address);

    // TODO - Maybe depending on config file, this should optionally print to STDERR or to a log
    // But as the intent is to run this in a container STDOUT is our best option.
    // TODO - output is a little rough looking, format better
    // TODO - Add datetime to this output to make this more useful.
    println!("Sending wol packet to {:02X?}", mac_address);

    // Send magic packet and alert on error
    magic_packet.send()?;
    Ok(())
}

pub fn read_config(config_path: &String) {
    // open targets file
    let path = Path::new(&config_path);
    let fh = match File::open(path) {
        Err(why) => panic!(
            "Couldn't open config file {}: {}",
            path.display(),
            why
        ),
        Ok(fh) => fh,
    };
    // sloppy regex so I can capture each byte.
    // look for a mac address with bytes delimited by a dot, dash, colon, space or nothing
    // TODO - do a split function or something more efficient than a regex
    // TODO - we could loop on the regex iterator instead of using bufreader
    // TODO - we should be checking the results before unwrapping
    let re= Regex::new(r"^([0-9a-fA-F]{2})[\s:\.\-]?([0-9a-fA-F]{2})[\s:\.\-]?([0-9a-fA-F]{2})[\s:\.\-]?([0-9a-fA-F]{2})[\s:\.\-]?([0-9a-fA-F]{2})[\s:\.\-]?([0-9a-fA-f]{2})$").unwrap();
    // for each line in the file, parse a mac address into a byte array (mac) and send_magic_packet to it.
    for line in BufReader::new(fh).lines() {
        for cap in re.captures_iter(&line.unwrap()) {
            // TODO - need to loop to create mac array, this is just embarrassing.
            let mac: [u8; 6] = [
                u8::from_str_radix(&cap[1], 16).unwrap(),
                u8::from_str_radix(&cap[2], 16).unwrap(),
                u8::from_str_radix(&cap[3], 16).unwrap(),
                u8::from_str_radix(&cap[4], 16).unwrap(),
                u8::from_str_radix(&cap[5], 16).unwrap(),
                u8::from_str_radix(&cap[6], 16).unwrap(),
            ];
            //println!("[{:02X?}:{:02X?}:{:02X?}:{:02X?}:{:02X?}:{:02X?}]", &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);
            // TODO - we need to build an array of mac arrays and return those
            //      instead of calling from in here
            if let Err(e) = send_magic_packet(mac) {
                println!("Error sending packet to {:02X?}\n{}", mac, e);
            }
        }
    }
}
