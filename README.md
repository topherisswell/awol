# AWOL
AWOL is a tool for sending Wake-on-LAN magic packets out at regular intervals to a list of MAC addresses. If you have a system that automatically boots when power comes on, and another system that doesn't, you can have the first running this program as a service to start the other after a power outage.

While this technically sends the wake-up packets in the order they're specified in the targets file, they are sent nearly simultaneously and this program currently does not provide any built-in method of staggering boot-ups.

It currently builds a statically linked binary with no dependencies. To build it you will need 2018 edition of Rust.

This is my first project in Rust and it shows. This program in its current state is not efficient and should not be expected to scale. This project is still in development and is not production ready. Use at your own risk. Much of the code is written with the expectation that there will be no more than a few dozen machines to wake, scaling beyond that may cause you problems.

## Usage
Put the list of MAC addresses you wish to wake up in a file called "targets" in the directory you run awol. The interval is currently hardcoded into the src/libs.rs file, and isn't configurable after compile-time.

There are no command-line flags at this time.

## Targets file format
Each MAC address should be on its own line. Bytes can be separated by dots (.), dashes (-), colons (:), spaces ( ), or nothing, but only one delimiter is tolerated. 

Hex letter can be upper or lowercase.

An example targets file is included in the repo.

#### Valid config
```
ab:cd:ef:01:23:45
67.89.ab.cd.ef.01
23-45-67-89-ab-cd
ef 01 23 45 67 89
0123456789ab
cdef.01 23-45:67
```

#### Invalid config
```
ab::cd::ef::01::23::45
67:89:ab:cd:ef:01   # Dave from accounting's coffee machine
12:23:34:45:56:67, 78:89:9a:ab:bc:cd
```
