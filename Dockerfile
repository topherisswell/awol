FROM docker.io/library/rust:latest AS builder
LABEL maintainer="christopher@topherisswell.com"
RUN mkdir -p /build/src
COPY Cargo* /build
COPY src /build/src
WORKDIR /build
RUN pwd && ls && cargo build --release

# TODO - make this from scratch and create a /config directory
FROM docker.io/library/alpine:latest
COPY --from=builder /build/target/release/awol /bin
RUN mkdir /config
VOLUME /config
COPY targets /config
USER 1000:1000
WORKDIR /config
CMD /bin/awol

